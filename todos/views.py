from django.shortcuts import render, get_list_or_404
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    todo_lists_list = get_list_or_404(TodoList)
    context = {
        "todo_list": todo_lists_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_list_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,

    }
    return render(request, "todos/detail.html", context)
